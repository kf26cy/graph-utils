/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 29/11/15.
 */
public class Graphs {


    /**
     * Generic functional interface. Applies a basic mathematical function on 2 operands.
     * @param <T>
     */
    interface Operand<T> {
        T applyOperand(T op1, T op2);
    }


    /**
     * <p>Computes the maximum sum path.<br>
     * Uses a bottom-up approach the dynamic programming paradigm.</p>
     *
     * <p>Note: Should <b>not</b> have any side effects onto the given graph.</p>
     *
     * <p>Given the following graph:</p>
     *
     * 3 <br>
     * 7 4 <br>
     * 2 4 6 <br>
     * 8 5 9 3 <br>
     *
     * <p>The way it is determined: <br>
     *    From 2 you can either go to 8 and have a partial sum of 10, or to to 5 and have a partial sum of 7. Since
     *    10 is greater than 7, 10 is preferred and the flattened array will start with 10. This logic continues with
     *    the other items, until the current array ends.</p>
     *
     * The graph now looks like: <br>
     *
     * 03 <br>
     * 07 04 <br>
     * 10 13 15 <br>
     *
     *
     * @param graph     containing {@code Number} elements
     * @param operand   mathematical function to be applied in calculating the "sum". Can be substituded with
     *                  multiplication obtaining the maximum product.
     *
     * @return the maximum sum obtained traversing the graph top-down.
     */
    public static <T extends Number & Comparable<T>> T maximumPathSum(final T[][] graph, final Operand<T> operand) {
        if (graph == null)              { throw new IllegalArgumentException("Graph cannot be null"); }
        if (graph.length == 1)          { return graph[0][0]; }
        if (!isGraphTriangular(graph))  { throw new IllegalArgumentException("Graph must be triangular in order to compute this sum."); }

        T[][] copy = shallowClone(graph);

        for (int depth = graph.length - 1; depth > 0; depth --) {
            flatten(copy[depth - 1], copy[depth], operand);
            copy[depth] = null;
        }

        return copy[0][0];
    }


    /**
     * Flattens in place (with side-effects) 2 arrays.
     * The results are stored in the "above" array.
     */
    private static <T extends Number & Comparable<T>> void flatten(T[] above, T[] beneath, Operand<T> operand) {
        for (int i = 0; i < above.length; i ++) {
            T south = operand.applyOperand(above[i], beneath[i]);
            T southEast = operand.applyOperand(above[i], beneath[i + 1]);
            above[i] = south.compareTo(southEast) > 0 ? south : southEast;
        }
    }


    private static <T> boolean isGraphTriangular(T[][] graph) {
        if (graph == null)                          { return false; }
        if (graph.length == 0 || graph.length == 1) { return true; }

        for (int depth = 0; depth < graph.length; depth ++) {
            if (graph[depth] == null || graph[depth].length != depth + 1) {
                return false;
            }
        }
        return true;
    }


    /**
     * <p>Performs a shallow copy of the given graph.<br>
     * Uses {@link Object#clone()} method.</p>
     */
    private static <T extends Number & Comparable<T>> T[][] shallowClone(T[][] graph) {
        T[][] _copy = graph.clone();
        for (int i = 0; i < graph.length; i ++) {
            _copy[i] = graph[i].clone();
        }
        return _copy;
    }


}
