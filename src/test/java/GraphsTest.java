import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 29/11/15.
 */
public class GraphsTest {


    @org.junit.Test
    public void testMaximumSumForDoubles() throws Exception {
        Double[][] graph = {
                {3d},
                {7d, 4d},
                {2d, 4d, 6d},
                {8d, 5d, 9d, 3d}
        };
        Graphs.Operand<Double> operand = (a, b) -> a + b;

        assertEquals(new Double(23), Graphs.maximumPathSum(graph, operand));
    }


    @org.junit.Test
    public void testMaximumSumForIntegers() throws Exception {
        Integer[][] graph = {
                {3},
                {7, 4},
                {2, 4, 6},
                {8, 5, 9, 3}
        };
        Graphs.Operand<Integer> operand = (a, b) -> a + b;

        assertEquals(new Integer(23), Graphs.maximumPathSum(graph, operand));
    }


    @org.junit.Test
    public void testGraph1Node() throws Exception {
        Integer[][] graph = {
                {3}
        };
        Graphs.Operand<Integer> operand = (a, b) -> a + b;

        assertEquals(new Integer(3), Graphs.maximumPathSum(graph, operand));
    }


    @org.junit.Test(expected = IllegalArgumentException.class)
    public void testForInvalidTriangleMatrix() throws Exception {
        Integer[][] graph = {
                {3},
                {7, 4},
                {2, 4, 6},
                {8, 5, 9}
        };
        Graphs.Operand<Integer> operand = (a, b) -> a + b;

        assertEquals(new Integer(23), Graphs.maximumPathSum(graph, operand));
    }


    @org.junit.Test
    public void testForSideEffects() throws Exception {
        Integer[][] graph = {
                {3},
                {7, 4},
                {2, 4, 6},
                {8, 5, 9, 3}
        };
        Graphs.maximumPathSum(graph, (a, b) -> a + b);

        Integer[][] graphCopy = {
                {3},
                {7, 4},
                {2, 4, 6},
                {8, 5, 9, 3}
        };
        assertArrayEquals(graph, graphCopy);
    }

}